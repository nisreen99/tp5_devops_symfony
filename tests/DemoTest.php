<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Demo;

class DemoTest extends TestCase
{
    public function testDemo()
    {
	    $demo = new Demo();
	    $demo->setDemo('demo');
	    
    	$this->assertTrue($demo->getDemo() === 'demo');
    }
}
